﻿using AutoReservation.BusinessLayer.Exceptions;
using AutoReservation.Dal;
using AutoReservation.Dal.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;

[assembly: CLSCompliant(true)]
namespace AutoReservation.BusinessLayer
{
    public abstract class ManagerBase<T>
        : IDisposable
        where T : class, IAbstractDomainObject
    {
        private Mutex updateLock = new Mutex();

        public ICollection<T> List {
            get
            {
                using (AutoReservationContext context = new AutoReservationContext())
                {
                    var list = GetDbSet(context).ToList();
                    list.ForEach(e => LoadDependencies(e, context));
                    return list;
                }
            }
        }
        
        /// <summary>
        /// Saves a new entity.
        /// </summary>
        /// <param name="entity">The new entity to save</param>
        /// <returns>ID of the created entity</returns>
        public int Create(T entity)
        {
            Validate(entity);

            using (AutoReservationContext context = new AutoReservationContext())
            {
                GetDbSet(context).Add(entity);
                context.SaveChanges();
                return entity.Id;
            }
        }

        public T Read(int id)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                var entity = GetDbSet(context).Find(id);
                if (entity != null)
                {
                    LoadDependencies(entity, context);
                }
                return entity;
            }
        }

        protected virtual void LoadDependencies(T entity, AutoReservationContext context) { }

        /// <summary>
        /// Saves an updated entity.
        /// </summary>
        /// <param name="entity">The updated entity to save</param>
        /// <returns>true, if the update was successful</returns>
        public bool Update(T entity)
        {
            Validate(entity);

            using (AutoReservationContext context = new AutoReservationContext())
            {
                var existing = GetDbSet(context).Find(entity.Id);
                if (existing == null)
                {
                    throw new ArgumentException("No entity with id " + entity.Id + " found.");
                }

                updateLock.WaitOne();

                try
                {
                    if (existing.RowVersion != entity.RowVersion)
                    {
                        throw CreateOptimisticConcurrencyException<T>(context, existing);
                    }

                    entity.RowVersion++;
                    DoUpdate(context, existing, entity);
                    return context.SaveChanges() > 0;
                }
                finally
                {
                    updateLock.ReleaseMutex();
                }
            }
        }

        protected virtual void DoUpdate(AutoReservationContext context, T existing, T entity)
        {
            context.Entry(existing).CurrentValues.SetValues(entity);
        }

        /// <summary>
        /// Deletes an entity.
        /// </summary>
        /// <param name="entity">The entity to delete</param>
        /// <returns>true, if the deletion was successful</returns>
        public bool Delete(T entity)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                context.Entry(entity).State = EntityState.Deleted;
                return context.SaveChanges() > 0;
            }
        }

        protected abstract DbSet<T> GetDbSet(AutoReservationContext context);

        protected internal virtual void Validate(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            return;
        }

        protected static OptimisticConcurrencyException<TEntityType> CreateOptimisticConcurrencyException<TEntityType>(DbContext context, TEntityType entity)
            where TEntityType : class
        {
            var dbEntity = (TEntityType)context?.Entry(entity)
                .GetDatabaseValues()
                .ToObject();

            return new OptimisticConcurrencyException<TEntityType>($"Update {typeof(TEntityType).Name}: Concurrency-Fehler", dbEntity);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                updateLock.Dispose();
            }
        }
    }
}