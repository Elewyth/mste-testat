﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace AutoReservation.BusinessLayer.Exceptions
{
    [Serializable]
    [SuppressMessage("Microsoft.Design", "CA1032:ImplementStandardExceptionConstructors")]
    public class InvalidDateRangeException
        : Exception
    {
        public InvalidDateRangeException() : base() { }
        public InvalidDateRangeException(string message) : base(message) { }
    }
}
