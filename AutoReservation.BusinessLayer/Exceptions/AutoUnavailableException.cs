﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace AutoReservation.BusinessLayer.Exceptions
{
    [Serializable]
    [SuppressMessage("Microsoft.Design", "CA1032:ImplementStandardExceptionConstructors")]
    public class AutoUnavailableException
        : Exception
    {
        public AutoUnavailableException() : base() { }
        public AutoUnavailableException(string message) : base(message) {}
    }
}
