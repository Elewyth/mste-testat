﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace AutoReservation.BusinessLayer.Exceptions
{
    [Serializable]
    [SuppressMessage("Microsoft.Usage", "CA2240:ImplementISerializableCorrectly")]
    public class OptimisticConcurrencyException<T> : Exception
    {
        public OptimisticConcurrencyException() { }

        public OptimisticConcurrencyException(string message) : base(message) { }
        public OptimisticConcurrencyException(string message, T mergedEntity) : base(message)
        {
            MergedEntity = mergedEntity;
        }

        public OptimisticConcurrencyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected OptimisticConcurrencyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public T MergedEntity { get; set; }
    }
}