﻿using AutoReservation.Dal;
using AutoReservation.Dal.Entities;
using System;
using System.Data.Entity;

namespace AutoReservation.BusinessLayer
{
    public class AutoManager
        : ManagerBase<Auto>
    {
        protected override DbSet<Auto> GetDbSet(AutoReservationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            return context.Autos;
        }

        protected override void DoUpdate(AutoReservationContext context, Auto existing, Auto entity)
        {
            if (existing.GetType() == entity.GetType())
            {
                base.DoUpdate(context, existing, entity);
            }
            else
            {
                context.Entry(existing).State = EntityState.Deleted;
                GetDbSet(context).Add(entity);
            }
        }
    }
}