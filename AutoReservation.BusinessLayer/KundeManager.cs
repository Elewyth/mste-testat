﻿using System;
using AutoReservation.Dal;
using AutoReservation.Dal.Entities;
using System.Data.Entity;

namespace AutoReservation.BusinessLayer
{
    public class KundeManager
        : ManagerBase<Kunde>
    {
        protected override DbSet<Kunde> GetDbSet(AutoReservationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            return context.Kunden;
        }
    }
}