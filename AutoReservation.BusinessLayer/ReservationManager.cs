﻿using AutoReservation.BusinessLayer.Exceptions;
using AutoReservation.Dal;
using AutoReservation.Dal.Entities;
using System;
using System.Data.Entity;
using System.Linq;

namespace AutoReservation.BusinessLayer
{
    public class ReservationManager
        : ManagerBase<Reservation>
    {
        protected override DbSet<Reservation> GetDbSet(AutoReservationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            return context.Reservationen;
        }

        protected internal override void Validate(Reservation entity)
        {
            base.Validate(entity);

            if (entity.Bis < entity.Von)
            {
                throw new InvalidDateRangeException("The reservation's end date must not be before the start date");
            }

            if ((entity.Bis - entity.Von) < TimeSpan.FromHours(24))
            {
                throw new InvalidDateRangeException("A reservation must be 24h or longer");
            }

            using (AutoReservationContext context = new AutoReservationContext())
            {
                var reservations = GetDbSet(context).Where(r => r.AutoId == entity.AutoId && r.ReservationsNr != entity.ReservationsNr);
                foreach (var reservation in reservations)
                {
                    if (reservation.Von < entity.Bis && reservation.Bis > entity.Von)
                    {
                        if (entity.Von > reservation.Von)
                        {
                            throw new AutoUnavailableException("The selected car is not available before " + reservation.Bis);
                        }
                        else
                        {
                            throw new AutoUnavailableException("The selected car is not available after " + reservation.Von);
                        }
                    }
                }
            }
        }

        protected override void LoadDependencies(Reservation entity, AutoReservationContext context)
        {
            base.LoadDependencies(entity, context);

            context.Entry(entity).Reference(r => r.Auto).Load();
            context.Entry(entity).Reference(r => r.Kunde).Load();
        }
    }
}