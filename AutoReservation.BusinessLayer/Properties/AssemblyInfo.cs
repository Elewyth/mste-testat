﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("AutoReservation.BusinessLayer")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("AutoReservation.BusinessLayer")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("465a39bf-6d9d-448d-8fdc-0e5e29e961a4")]

// Allow unit-tests to access internal fields and methods
[assembly: InternalsVisibleTo("AutoReservation.BusinessLayer.Testing, PublicKey=0024000004800000940000000602000000240000525341310004000001000100af4064d5e97b4de186982babac71895ca6a4f3f5fc58d4184c58dabded6876ea1080c2702f1f4f48dde922ea26723deaee87b0e03f3d207ad999ec28a5b94c7f241830467e119ac0fc505a4731c0df277e91ce0b83dd4b9e9995cebfa59e3eb1d0de0067d2388e2777405115c5d771ac13678cd95a70f036f3a53af00e6154e9")]


