﻿using AutoReservation.Dal;
using System;
using System.ServiceModel;

[assembly: CLSCompliant(true)]
namespace AutoReservation.Service.Wcf.Host
{
    public static class Program
    {
        static void Main()
        {
            Console.WriteLine("AutoReservationService starting.");

            // Instantiate new ServiceHost 
            using (ServiceHost host = new ServiceHost(typeof(AutoReservationService)))
            {

                // Initialize Database
                Console.WriteLine("Initializing Database Context.");
                using (var context = new AutoReservationContext())
                {
                    Console.WriteLine("Database Context initialized.");
                }

                // Open ServiceHost
                host.Open();

                Console.WriteLine("AutoReservationService started.");
                Console.WriteLine();
                Console.WriteLine("Press Return to stop the Service.");

                Console.ReadLine();

                // Stop ServiceHost
                if (host.State != CommunicationState.Closed)
                {
                    host.Close();
                }
            }
        }
    }
}
