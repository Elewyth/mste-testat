﻿using AutoReservation.Common.DataTransferObjects;
using AutoReservation.Common.Interfaces;
using AutoReservation.Service.Wcf;
using AutoReservation.TestEnvironment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ServiceModel;

[assembly: CLSCompliant(true)]
namespace AutoReservation.Service.Wcf.Testing
{
    [TestClass]
    public abstract class ServiceTestBase
    {
        protected abstract IAutoReservationService Target { get; }

        [TestInitialize]
        public void InitializeTestData()
        {
            TestEnvironmentHelper.InitializeTestData();
        }

        #region Read all entities

        [TestMethod]
        public void GetAutosTest()
        {
            // Assert.Inconclusive("Test not implemented.");
            var result = Target.ReadAllAutos();
            Assert.AreEqual(3, result.Count);
        }

        [TestMethod]
        public void GetKundenTest()
        {
            // Assert.Inconclusive("Test not implemented.");
            var result = Target.ReadAllKunden();
            Assert.AreEqual(4, result.Count);
        }

        [TestMethod]
        public void GetReservationenTest()
        {
            var result = Target.ReadAllReservationen();
            Assert.AreEqual(4, result.Count);
        }

        #endregion

        #region Get by existing ID

        [TestMethod]
        public void GetAutoByIdTest()
        {
            //Assert.Inconclusive("Test not implemented.");
            var result = Target.ReadAutos(1);
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }

        [TestMethod]
        public void GetKundeByIdTest()
        {
            // Assert.Inconclusive("Test not implemented.");
            var result = Target.ReadKunden(1);
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }

        [TestMethod]
        public void GetReservationByNrTest()
        {
            var result = Target.ReadReservation(1);
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.ReservationsNr);
        }

        #endregion

        #region Get by not existing ID

        [TestMethod]
        public void GetAutoByIdWithIllegalIdTest()
        {
            //Assert.Inconclusive("Test not implemented.");
            var resullt = Target.ReadAutos(5);
            Assert.IsNull(resullt);
        }

        [TestMethod]
        public void GetKundeByIdWithIllegalIdTest()
        {
            // Assert.Inconclusive("Test not implemented.");
            var result = Target.ReadKunden(5);
            Assert.IsNull(result);
        }

        [TestMethod]
        public void GetReservationByNrWithIllegalIdTest()
        {
            var result = Target.ReadReservation(5);
            Assert.IsNull(result);
        }

        #endregion

        #region Insert

        [TestMethod]
        public void InsertAutoTest()
        {
          //  Assert.Inconclusive("Test not implemented.");
            
            var result = Target.CreateAuto(new AutoDto()
            {
                Id = 1,
                Marke =  "Audi",
               // AutoKlasse = new AutoKlasse { "Luxusklasse" },
                Tagestarif =  5,
                Basistarif = 4
            });

            Assert.AreEqual(4, result);
            Assert.IsNotNull(Target.ReadAutos(result));
            
        }

        [TestMethod]
        public void InsertKundeTest()
        {
         //   Assert.Inconclusive("Test not implemented.");
            
            var result = Target.CreateKunde(new KundeDto()
            {
                Id =  4,
                Nachname = "Nachname",
                Vorname = "Vorname",
                Geburtsdatum = new DateTime ( 1980, 01, 01)
            });

            Assert.AreEqual(5, result);
            Assert.IsNotNull(Target.ReadKunden(result));
        
        }

        [TestMethod]
        public void InsertReservationTest()
        {
            var result = Target.CreateReservation(new ReservationDto()
            {
                Auto = new AutoDto { Id = 1 },
                Kunde = new KundeDto { Id = 1 },
                Von = new DateTime(2017, 11, 20),
                Bis = new DateTime(2017, 11, 21)
            });

            Assert.AreEqual(5, result);
            Assert.IsNotNull(Target.ReadReservation(result));
        }

        #endregion

        #region Delete  

        [TestMethod]
        public void DeleteAutoTest()
        {
            // Assert.Inconclusive("Test not implemented.");
            var result = Target.DeleteAuto(Target.ReadAutos(1));
            Assert.IsTrue(result);
            Assert.IsNull(Target.ReadAutos(1));
        }

        [TestMethod]
        public void DeleteKundeTest()
        {
            // Assert.Inconclusive("Test not implemented.");
            var result = Target.DeleteKunde(Target.ReadKunden(1));
            Assert.IsTrue(result);
            Assert.IsNull(Target.ReadKunden(1));
        }

        [TestMethod]
        public void DeleteReservationTest()
        {
            var result = Target.DeleteReservation(Target.ReadReservation(1));
            Assert.IsTrue(result);
            Assert.IsNull(Target.ReadReservation(1));
        }

        #endregion

        #region Update

        [TestMethod]
        public void UpdateAutoTest()
        {
            // Assert.Inconclusive("Test not implemented.");
            var result = Target.ReadAutos(1);
            result.Marke = "Ken";
            var res = Target.UpdateAuto(result);
            Assert.IsTrue(res);
            Assert.AreEqual(result.Marke.ToString(), Target.ReadAutos(1).Marke.ToString());
            Assert.AreEqual(result.RowVersion + 1, Target.ReadAutos(1).RowVersion);
        }

        [TestMethod]
        public void UpdateKundeTest()
        {
            // Assert.Inconclusive("Test not implemented.");
            var result = Target.ReadKunden(1);
            result.Nachname = "nAmE"; // Beil
            var res = Target.UpdateKunde(result);
            Assert.IsTrue(res);
            Assert.AreEqual(result.Nachname.ToString(), Target.ReadKunden(1).Nachname.ToString());
            Assert.AreEqual(result.RowVersion + 1, Target.ReadKunden(1).RowVersion);
        }

        [TestMethod]
        public void UpdateReservationTest()
        {
            var reservation = Target.ReadReservation(1);
            reservation.Von = DateTime.Now;
            var result = Target.UpdateReservation(reservation);
            Assert.IsTrue(result);
            Assert.AreEqual(reservation.Von.ToString(), Target.ReadReservation(1).Von.ToString());
            Assert.AreEqual(reservation.RowVersion + 1, Target.ReadReservation(1).RowVersion);
        }

        #endregion

        #region Update with optimistic concurrency violation

        [TestMethod]
        [ExpectedException(typeof(FaultException), AllowDerivedTypes = true)]
        public void UpdateAutoWithOptimisticConcurrencyTest()
        {
            //Assert.Inconclusive("Test not implemented.");
            var result = Target.ReadAutos(1);
            Target.UpdateAuto(result);
            Target.UpdateAuto(result);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException), AllowDerivedTypes = true)]
        public void UpdateKundeWithOptimisticConcurrencyTest()
        {
            //Assert.Inconclusive("Test not implemented.");
            var result = Target.ReadKunden(1);
            Target.UpdateKunde(result);
            Target.UpdateKunde(result);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException), AllowDerivedTypes = true)]
        public void UpdateReservationWithOptimisticConcurrencyTest()
        {
            var reservation = Target.ReadReservation(1);
            Target.UpdateReservation(reservation);
            Target.UpdateReservation(reservation);
        }

        #endregion

        #region Insert / update invalid time range

        [TestMethod]
        [ExpectedException(typeof(FaultException), AllowDerivedTypes = true)]
        public void InsertReservationWithInvalidDateRangeTest()
        {
            Target.CreateReservation(new ReservationDto
            {
                Von = new DateTime(2017, 11, 20),
                Bis = new DateTime(2017, 11, 19)
            });
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException), AllowDerivedTypes = true)]
        public void UpdateReservationWithInvalidDateRangeTest()
        {
            Target.UpdateReservation(new ReservationDto
            {
                ReservationsNr = 1,
                Von = new DateTime(2017, 11, 20),
                Bis = new DateTime(2017, 11, 20, 12, 0, 0)
            });
        }

        #endregion

        #region Check Availability

        [TestMethod]
        [ExpectedException(typeof(FaultException), AllowDerivedTypes = true)]
        public void InsertReservationWithAutoNotAvailableTest()
        {
            Target.CreateReservation(new ReservationDto
            {
                Auto = new AutoDto { Id = 1 },
                Von = new DateTime(2020, 1, 15),
                Bis = new DateTime(2020, 1, 25)
            });
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException), AllowDerivedTypes = true)]
        public void UpdateReservationWithAutoNotAvailableTest()
        {
            Target.UpdateReservation(new ReservationDto
            {
                ReservationsNr = 1,
                Auto = new AutoDto { Id = 1 },
                Von = new DateTime(2020, 2, 15),
                Bis = new DateTime(2020, 2, 25)
            });
        }

        #endregion
    }
}
