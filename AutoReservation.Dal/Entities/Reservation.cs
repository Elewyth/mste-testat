using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AutoReservation.Dal.Entities
{

    [Table("Reservationen")]
    public class Reservation
        : IAbstractDomainObject
    {
        [NotMapped]
        public int Id {
            get
            {
                return ReservationsNr;
            }

            set
            {
                ReservationsNr = value;
            }
        }

        [Key]
        public int ReservationsNr { get; set; }

        [Required]
        public DateTime Von { get; set; }

        [Required]
        public DateTime Bis { get; set; }
        
        [Required]
        public int AutoId { get; set; } 
        public virtual Auto Auto { get; set; }
        
        [Required]
        public int KundeId { get; set; }
        public virtual Kunde Kunde { get; set; }

        [Required]
        public int RowVersion { get; set; }
    }
}

