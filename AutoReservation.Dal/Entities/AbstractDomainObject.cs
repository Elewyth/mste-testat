﻿using System;

namespace AutoReservation.Dal.Entities
{
    public interface IAbstractDomainObject
    {
        int Id { get; set; }
        int RowVersion { get; set; }
    }
}
