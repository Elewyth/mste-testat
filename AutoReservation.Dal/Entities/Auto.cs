using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace AutoReservation.Dal.Entities
{
    [Table("Autos")]
    public class Auto
        : IAbstractDomainObject
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public String Marke { get; set; }

        [Required]
        public int Tagestarif { get; set; }

        [Required]
        public int RowVersion { get; set; }
    }
}
