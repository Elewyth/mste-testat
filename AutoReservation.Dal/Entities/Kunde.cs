using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace AutoReservation.Dal.Entities
{
    [Table("Kunden")]
    public class Kunde
        : IAbstractDomainObject
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public String Nachname { get; set; }

        [Required]
        public String Vorname { get; set; }

        [Required]
        public DateTime Geburtsdatum { get; set; }

        [Required]
        public int RowVersion { get; set; }
    }
}
