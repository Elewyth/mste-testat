using System.Data.Entity.Migrations;

namespace AutoReservation.Dal.Migrations
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    internal sealed class Configuration : DbMigrationsConfiguration<AutoReservationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = typeof(AutoReservationContext).FullName;
        }
    }
}
