﻿using AutoReservation.Common.DataTransferObjects;
using AutoReservation.Common.Interfaces;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using AutoReservation.BusinessLayer;
using AutoReservation.Dal.Entities;
using AutoReservation.BusinessLayer.Exceptions;
using AutoReservation.Common.DataTransferObjects.Faults;
using System.Linq;

namespace AutoReservation.Service.Wcf
{
    public class AutoReservationService
        : IAutoReservationService
    {

        private ManagerBase<Reservation> reservationManager = new ReservationManager();
        private ManagerBase<Auto> autoManager = new AutoManager();
        private ManagerBase<Kunde> kundeManager = new KundeManager();

        private static void WriteActualMethod() 
            => Console.WriteLine($"Calling: {new StackTrace().GetFrame(1).GetMethod().Name}");

        #region Auto



        public int CreateAuto(AutoDto auto)
        {
            WriteActualMethod();
            try
            {
                return autoManager.Create(auto.ConvertToEntity());
            }
            catch (InvalidProgramException ex)
            {
                throw new InvalidProgramException(ex.Source);
            }
            
        }



        public ICollection<AutoDto> ReadAllAutos()
        {
            WriteActualMethod();
            return autoManager.List.ConvertToDtos();
        }

      /*  public AutoDto ReadAuto(int id)
        {
            WriteActualMethod();
            return autoManager.Read(id).ConvertToDto();
        }
*/
        public ICollection<AutoDto> ReadAllAutoFields(AutoDto auto)
        {
            WriteActualMethod();
            return autoManager.List.Where(r => r.Id == auto.Id).ConvertToDtos();
        }

        public AutoDto ReadAutos(int id)
        {
            WriteActualMethod();
            return autoManager.Read(id).ConvertToDto();
        }

        public bool UpdateAuto(AutoDto auto)
        {
            WriteActualMethod();
            try
            {
                return autoManager.Update(auto.ConvertToEntity());
            }
            catch (OptimisticConcurrencyException<Auto> ex)
            {
                throw new OptimisticConcurrencyFaultException<AutoDto>(ex.Message, ex.MergedEntity.ConvertToDto());
            }
        }

        public bool DeleteAuto(AutoDto auto)
        {
            WriteActualMethod();
            return autoManager.Delete(auto.ConvertToEntity());
        }

        #endregion
        #region Kunde

        public int CreateKunde(KundeDto kunde)
        {
            WriteActualMethod();
            try
            {
                return kundeManager.Create(kunde.ConvertToEntity());
            }
            catch (InvalidDateRangeException ex)
            {
                throw new InvalidDateRangeException(ex.Message);
            }
        }

        public ICollection<KundeDto> ReadAllKunden()
        {
            WriteActualMethod();
            return kundeManager.List.ConvertToDtos();
        }

       /* public KundeDto ReadKunde(int id)
        {
            WriteActualMethod();
            return kundeManager.Read(id).ConvertToDto(); 
        }
        */

        public ICollection<KundeDto> ReadAllKundeFields(KundeDto kunde)
        {
            WriteActualMethod();
            return kundeManager.List.Where(r => r.Id == kunde.Id).ConvertToDtos();
        }

        public KundeDto ReadKunden(int id)
        {
            WriteActualMethod();
            return kundeManager.Read(id).ConvertToDto();
        }

        public bool UpdateKunde(KundeDto kunde)
        {
            WriteActualMethod();
            try
            {
                return kundeManager.Update(kunde.ConvertToEntity());
            }
            catch (OptimisticConcurrencyException<Kunde> ex)
            {
                throw new OptimisticConcurrencyFaultException<KundeDto>(ex.Message, ex.MergedEntity.ConvertToDto());
            }
           
        }

        public bool DeleteKunde(KundeDto kunde)
        {
            WriteActualMethod();
            return kundeManager.Delete(kunde.ConvertToEntity());
        }

        #endregion
        #region Reservation

        public int CreateReservation(ReservationDto reservation)
        {
            WriteActualMethod();
            try
            {
                return reservationManager.Create(reservation.ConvertToEntity());
            }
            catch (InvalidDateRangeException ex)
            {
                throw new InvalidDateRangeFaultException(ex.Message);
            }
            catch (AutoUnavailableException ex)
            {
                throw new AutoUnavailableFaultException(ex.Message);
            }
        }

        public ICollection<ReservationDto> ReadAllReservationen()
        {
            WriteActualMethod();
            return reservationManager.List.ConvertToDtos();
        }

        public ICollection<ReservationDto> ReadAllReservationenForAuto(AutoDto auto)
        {
            WriteActualMethod();
            return reservationManager.List.Where(r => r.AutoId == auto.Id).ConvertToDtos();
        }

        public ReservationDto ReadReservation(int id)
        {
            WriteActualMethod();
            return reservationManager.Read(id).ConvertToDto();
        }

        public bool UpdateReservation(ReservationDto reservation)
        {
            WriteActualMethod();

            try
            {
                return reservationManager.Update(reservation.ConvertToEntity());
            }
            catch (OptimisticConcurrencyException<Reservation> ex)
            {
                throw new OptimisticConcurrencyFaultException<ReservationDto>(ex.Message, ex.MergedEntity.ConvertToDto());
            }
            catch (InvalidDateRangeException ex)
            {
                throw new InvalidDateRangeFaultException(ex.Message);
            }
            catch (AutoUnavailableException ex)
            {
                throw new AutoUnavailableFaultException(ex.Message);
            }
        }

        public bool DeleteReservation(ReservationDto reservation)
        {
            WriteActualMethod();
            return reservationManager.Delete(reservation.ConvertToEntity());
        }

        #endregion
    }
}