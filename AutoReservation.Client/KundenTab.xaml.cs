﻿using AutoReservation.Common.DataTransferObjects;
using System;
using System.Windows;
using System.Windows.Controls;

namespace AutoReservation.Client
{
    /// <summary>
    /// Interaction logic for KundenTab.xaml
    /// </summary>
    public partial class KundenTab : UserControl
    {
        public KundenTab()
        {
            InitializeComponent();
            ReloadList();
        }

        private void Button_Click_Customers(object sender, RoutedEventArgs e)
        => ReloadList(() => MessageBox.Show("Customers reloaded successfully.", "Reload successful"));

        private void Button_Add_Customers(object sender, RoutedEventArgs e)
        {
            KundenList.SelectedItem = null;
            Nachname.Text = "";
            Vorname.Text = "";
            GDatum.SelectedDate = null;
            GDatum.DisplayDate = DateTime.Now;
            SaveButton.IsEnabled = true;
            DeleteButton.IsEnabled = true;
            SetCustomerEditorVisible(true);
        }

        private void Button_Delete_Customers(object sender, RoutedEventArgs e)
        {
            var customer = ((KundeDto)KundenList.SelectedItem);
            if (customer == null)
            {
                ReloadList();
                return;
            }

            ServiceWrapper.Execute(service =>
            {
                if (service.DeleteKunde(customer))
                {
                    ReloadList(() => MessageBox.Show("Customer deleted successfully.", "Deletion successful"));
                }
                else
                {
                    MessageBox.Show("Customer could not be deleted", "Deletion faled");
                }
            });
        }

        private void Button_Save_Customers(object sender, RoutedEventArgs e)
        {
            var selectedItem = (KundeDto)KundenList.SelectedItem;
            if (selectedItem == null)
            {
                CreateCustomer();
            }
            else
            {
                UpdateCustomer(selectedItem);
            }
        }

        private void CreateCustomer()
        {
            var nachname = (String)Nachname.Text;
            if (nachname == null)
            {
                MessageBox.Show("Please write a value for \"Nachname\"", "Customer last name incomplete");
                return;
            }

            var vorname = (String)Vorname.Text;
            if (vorname == null)
            {
                MessageBox.Show("Please write a value for \"Vorname\"", "Customer fist name incomplete");
                return;
            }

            var geburtsdatum = GDatum.SelectedDate;
            if (geburtsdatum == null)
            {
                MessageBox.Show("Please select a value for \"Geburtsdatum\"", "Customer birthday incomplete");
                return;
            }

            ServiceWrapper.Execute(service =>
            {
                var customerNr = service.CreateKunde(new KundeDto
                {
                    Nachname = nachname,
                    Vorname = vorname,
                    Geburtsdatum = geburtsdatum.Value
                });

                if (customerNr > 0)
                {
                    ReloadList(() => MessageBox.Show("Customer created successfully. (Customer ID " + customerNr + ")", "Creation successful"));
                }
                else
                {
                    MessageBox.Show("Customer could not be created.", "Creation failed");
                }
            });
        }

        private void UpdateCustomer(KundeDto kunde)
        {
            ServiceWrapper.Execute(service =>
            {
                if (service.UpdateKunde(kunde))
                { 
                 ReloadList(() => MessageBox.Show("Customer saved successfully.", "Save successful"));
                }
                else
                {
                MessageBox.Show("Customer could not be saved.", "Save failed");
                }
             });
        }

        private void ReloadList() => ReloadList(() => { });

        private void ReloadList(Action callback)
        {
            ServiceWrapper.Execute(service =>
            {
                KundenList.ItemsSource = service.ReadAllKunden();
                SetCustomerEditorVisible(false);
                callback.Invoke();
            });
         
        }

        private void KundenList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var kunde = ((KundeDto)KundenList.SelectedItem);
            if(kunde != null)
            {
                ServiceWrapper.Execute(service =>
                {
                    kunde = service.ReadKunden(kunde.Id);
                    KundenList.SelectedItem = kunde;
                    SetCustomerEditorVisible(true);
                });
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void SetCustomerEditorVisible(bool visible)
        {
            if (visible)
            {
                KundeEditor.Visibility = Visibility.Visible;
                CustomerEditorEmptyHint.Visibility = Visibility.Collapsed;
                SaveButton.IsEnabled = true;
                DeleteButton.IsEnabled = true;
            }
            else
            {
                KundeEditor.Visibility = Visibility.Collapsed;
                CustomerEditorEmptyHint.Visibility = Visibility.Visible;
                SaveButton.IsEnabled = false;
                DeleteButton.IsEnabled = false;
            }
        }      
    }
}
