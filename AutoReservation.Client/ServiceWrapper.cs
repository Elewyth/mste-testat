﻿using AutoReservation.Common.Interfaces;
using System;
using System.ServiceModel;
using System.Windows;

namespace AutoReservation.Client
{
    public static class ServiceWrapper
    {
        private static EndpointAddress backendAddress = new EndpointAddress("net.tcp://localhost:7876/AutoReservationService");

        private static IAutoReservationService autoReservationService { get; set; }

        public delegate void ServiceCall(IAutoReservationService service);

        public static void Execute(ServiceCall serviceCall)
        {
            try
            {
                Connect();
                serviceCall.Invoke(autoReservationService);
            }
            catch (EndpointNotFoundException ex)
            {
                MessageBox.Show(ex.Message, "Endpoint not found");
                autoReservationService = null;
            }
            catch (CommunicationException ex)
            {
                MessageBox.Show(ex.Message, "Communication failure");
            }
        }

        private static void Connect()
        {
            if (autoReservationService != null)
            {
                return;
            }

            var factory = new ChannelFactory<IAutoReservationService>(new NetTcpBinding(), backendAddress);
            autoReservationService = factory.CreateChannel();
        }
    }
}
