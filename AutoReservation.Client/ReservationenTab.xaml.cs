﻿using AutoReservation.Common.DataTransferObjects;
using System;
using System.Windows;
using System.Windows.Controls;

namespace AutoReservation.Client
{
    /// <summary>
    /// Interaction logic for ReservationenTab.xaml
    /// </summary>
    public partial class ReservationenTab : UserControl
    {
        public ReservationenTab()
        {
            InitializeComponent();
            ReloadList();
        }

        private void ReloadButton_Click(object sender, RoutedEventArgs e)
            => ReloadList(() => MessageBox.Show("Reservations reloaded successfully.", "Reload successful"));

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            ReservationenList.SelectedItem = null;
            KundeSelector.SelectedItem = null;
            AutoSelector.SelectedItem = null;
            VonPicker.DisplayDate = DateTime.Now;
            BisPicker.DisplayDate = DateTime.Now.AddDays(1);
            SetReservationEditorVisible(true);
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            var reservation = ((ReservationDto)ReservationenList.SelectedItem);
            if (reservation == null)
            {
                ReloadList();
                return;
            }

            ServiceWrapper.Execute(service =>
            {
                if (service.DeleteReservation(reservation))
                {
                    ReloadList(() => MessageBox.Show("Reservation deleted successfully.", "Deletion successful"));
                }
                else
                {
                    MessageBox.Show("Reservation could not be deleted.", "Deletion failed");
                }
            });
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = (ReservationDto)ReservationenList.SelectedItem;
            if (selectedItem == null)
            {
                CreateReservation();
            }
            else
            {
                UpdateReservation(selectedItem);
            }
        }

        private void CreateReservation()
        {
            var kunde = (KundeDto)KundeSelector.SelectedItem;
            if (kunde == null)
            {
                MessageBox.Show("Please select a value for \"Kunde\"", "Reservation incomplete");
                return;
            }

            var auto = (AutoDto)AutoSelector.SelectedItem;
            if (auto == null)
            {
                MessageBox.Show("Please select a value for \"Auto\"", "Reservation incomplete");
                return;
            }

            var von = VonPicker.SelectedDate;
            if (von == null)
            {
                MessageBox.Show("Please select a value for \"Von\"", "Reservation incomplete");
                return;
            }

            var bis = BisPicker.SelectedDate;
            if (bis == null)
            {
                MessageBox.Show("Please select a value for \"Bis\"", "Reservation incomplete");
                return;
            }

            ServiceWrapper.Execute(service => {
                var reservationsNr = service.CreateReservation(new ReservationDto
                {
                    Kunde = kunde,
                    Auto = auto,
                    Von = von.Value,
                    Bis = bis.Value
                });

                if (reservationsNr > 0)
                {
                    ReloadList(() => MessageBox.Show("Reservation created successfully. (ReservationsNr " + reservationsNr + ")", "Creation successful"));
                }
                else
                {
                    MessageBox.Show("Reservation could not be created.", "Creation failed");
                }
            });
        }

        private void UpdateReservation(ReservationDto reservation)
        {
            reservation.Von = VonPicker.SelectedDate.Value;
            reservation.Bis = BisPicker.SelectedDate.Value;
            ServiceWrapper.Execute(service =>
            {
                if (service.UpdateReservation(reservation))
                {
                    ReloadList(() => MessageBox.Show("Reservation saved successfully.", "Save successful"));
                }
                else
                {
                    MessageBox.Show("Reservation could not be saved.", "Save failed");
                }
            });
        }

        private void ReloadList() => ReloadList(() => { });

        private void ReloadList(Action callback)
        {
            ServiceWrapper.Execute(service =>
            {
                ReservationenList.ItemsSource = service.ReadAllReservationen();
                KundeSelector.ItemsSource = service.ReadAllKunden();
                AutoSelector.ItemsSource = service.ReadAllAutos();
                SetReservationEditorVisible(false);
                callback.Invoke();
            });
        }

        private void ReservationenList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var reservation = ((ReservationDto)ReservationenList.SelectedItem);
            if (reservation != null)
            {
                ServiceWrapper.Execute(service =>
                {
                    reservation = service.ReadReservation(reservation.ReservationsNr);
                    ReservationenList.SelectedItem = reservation;
                    KundeSelector.SelectedValue = reservation.Kunde.Id;
                    AutoSelector.SelectedValue = reservation.Auto.Id;
                    UpdateDatePickers();
                    SetReservationEditorVisible(true);
                });
            }
        }

        private void KundeSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var reservation = ((ReservationDto)ReservationenList.SelectedItem);
            if (reservation != null)
            {
                reservation.Kunde = (KundeDto)KundeSelector.SelectedItem;
            }
        }

        private void AutoSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var reservation = ((ReservationDto)ReservationenList.SelectedItem);
            if (reservation != null)
            {
                reservation.Auto = (AutoDto)AutoSelector.SelectedItem;
            }
            UpdateDatePickers();
        }

        private void SetReservationEditorVisible(bool visible)
        {
            if (visible)
            {
                ReservationEditor.Visibility = Visibility.Visible;
                ReservationEditorEmptyHint.Visibility = Visibility.Collapsed;
                SaveButton.IsEnabled = true;
                DeleteButton.IsEnabled = true;
                UpdateDatePickers();
            }
            else
            {
                ReservationEditor.Visibility = Visibility.Collapsed;
                ReservationEditorEmptyHint.Visibility = Visibility.Visible;
                SaveButton.IsEnabled = false;
                DeleteButton.IsEnabled = false;
            }
        }

        private void UpdateDatePickers()
        {
            VonPicker.SelectedDate = null;
            BisPicker.SelectedDate = null;

            VonPicker.BlackoutDates.Clear();
            BisPicker.BlackoutDates.Clear();

            VonPicker.BlackoutDates.AddDatesInPast();
            BisPicker.BlackoutDates.AddDatesInPast();

            var auto = (AutoDto)AutoSelector.SelectedItem;
            if (auto != null)
            {
                ServiceWrapper.Execute(service =>
                {
                    foreach (var reservationForAuto in service.ReadAllReservationenForAuto(auto))
                    {
                        if (reservationForAuto.ReservationsNr != ((ReservationDto)ReservationenList.SelectedItem)?.ReservationsNr)
                        {

                            VonPicker.BlackoutDates.Add(new CalendarDateRange(reservationForAuto.Von, reservationForAuto.Bis));
                            BisPicker.BlackoutDates.Add(new CalendarDateRange(reservationForAuto.Von, reservationForAuto.Bis));
                        }
                    }
                });
            }

            var reservation = (ReservationDto)ReservationenList.SelectedItem;
            if (reservation != null)
            {
                try
                {
                    VonPicker.SelectedDate = reservation.Von;
                    BisPicker.SelectedDate = reservation.Bis;
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    MessageBox.Show("The selected reservation dates are not valid.", "Invalid reservation");
                }
            }
        }
    }
}
