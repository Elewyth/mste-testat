﻿using AutoReservation.Common.DataTransferObjects;
using System;
using System.Windows;
using System.Windows.Controls;

namespace AutoReservation.Client
{
    /// <summary>
    /// Interaction logic for AutoTab.xaml
    /// </summary>
    public partial class AutoTab : UserControl
    {
        public AutoTab()
        {
            InitializeComponent();
            ReloadList();
        }

        private void Button_Reload_Cars(object sender, RoutedEventArgs e)
        => ReloadList(() => MessageBox.Show("Cars reloaded successfully.", "Reload successful"));

        private void Button_Add_Cars(object sender, RoutedEventArgs e)
        {
            AutoList.SelectedItem = null;
            AutoKlasseSelector.SelectedItem = null;
            Marke.Text = "";
            Tagestarif.Text = "";
            Basistarif.Text = "";
            SaveButton.IsEnabled = true;
            DeleteButton.IsEnabled = true;
            SetCarEditorVisible(true);
        }

        private void Button_Delete_Cars(object sender, RoutedEventArgs e)
        {
            var auto = ((AutoDto)AutoList.SelectedItem);
            if (auto == null)
            {
                ReloadList();
                return;
            }

            ServiceWrapper.Execute(service =>
            {
                if (service.DeleteAuto(auto))
                {
                  ReloadList(() =>  MessageBox.Show("Car deleted successfully.", "Deletion successful"));
                }
                else
                {
                    MessageBox.Show("Car could not be deleted", "Deletion failed");
                }
            });
        }

        private void Button_Save_Cars(object sender, RoutedEventArgs e)
        {
            var selectedItem = (AutoDto)AutoList.SelectedItem;
            if (selectedItem == null)
            {
                CreateAuto();
            }
            else
            {
                UpdateAuto(selectedItem);
            }
        }

        private void CreateAuto()
        {

            var autoKlasse = AutoKlasseSelector.SelectedValue?.ToString();
            if (autoKlasse == null || autoKlasse == "")
            {
                MessageBox.Show("Please select a value for \"Klasse\"", "Car type incomplete");
                return;
            }

            var marke = Marke.Text;
            if (marke == null || marke == "")
            {
                MessageBox.Show("Please write a value for \"Marke\"", "Car brand incomplete");
                return;
            }

            int tagestarif;
            if (!Int32.TryParse(Tagestarif.Text, out tagestarif))
            {
                 MessageBox.Show("Please write a value for \"Tagestarif\"", "Car day rate incomplete");
                 return;
             }

            int basistarif = 0;
            if (autoKlasse == AutoKlasse.Luxusklasse.ToString() && !Int32.TryParse(Basistarif.Text, out basistarif))
            {
                MessageBox.Show("Please write a value for \"Basistarif\"", "Car base rate incomplete");
                return;
            }

            ServiceWrapper.Execute(service => 
            {
                var autoNr = service.CreateAuto(new AutoDto
                {
                    AutoKlasse = (AutoKlasse)Enum.Parse(typeof(AutoKlasse), autoKlasse),
                    Marke = marke,
                    Tagestarif = tagestarif,
                    Basistarif = basistarif
                });

                if (autoNr > 0)
                {
                    ReloadList(() => MessageBox.Show("Car created successfully. (Car Nr " + autoNr + ")", "Creation sucessful"));
                }
                else
                {
                    MessageBox.Show("Car could not be created.", "Creation failed");
                }
            });
            }

        private void UpdateAuto (AutoDto auto)
        {
            var autoKlasse = AutoKlasseSelector.SelectedValue.ToString();
            if (autoKlasse == null || autoKlasse == "")
            {
                MessageBox.Show("Please select a value for \"Klasse\"", "Car type incomplete");
                return;
            }

            auto.AutoKlasse = (AutoKlasse) Enum.Parse(typeof(AutoKlasse), autoKlasse);

            ServiceWrapper.Execute(service =>
            {
                if (service.UpdateAuto(auto))
                {
                 ReloadList(() => MessageBox.Show("Car saved successfully.", "Save successful"));
                }
                else
                {
                    MessageBox.Show("Car could not be saved.", "Save failed");
                }
            });
        }

        private void ReloadList() => ReloadList(() => { });
 
        private void ReloadList(Action callback)
        {
            ServiceWrapper.Execute(service =>
            {
                AutoList.ItemsSource = service.ReadAllAutos();
                SetCarEditorVisible(false);
                callback.Invoke();
            });
        }

        private void AutoList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var auto = ((AutoDto)AutoList.SelectedItem);
            if (auto != null)
            {
                ServiceWrapper.Execute(service =>
                {
                    auto = service.ReadAutos(auto.Id);
                    AutoList.SelectedItem = auto;
                    AutoKlasseSelector.SelectedValue = auto.AutoKlasse.ToString();
                    SetCarEditorVisible(true);
                });
            }
        }

        private void AutoKlasseSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateBasistarifVisibility();
        }

        private void UpdateBasistarifVisibility()
        {
            var autoKlasse = AutoKlasseSelector.SelectedValue?.ToString();
            if (autoKlasse == null)
            {
                BasistarifLabel.Visibility = Visibility.Hidden;
                Basistarif.Visibility = Visibility.Hidden;
                return;
            }

            if ((AutoKlasse)Enum.Parse(typeof(AutoKlasse), autoKlasse) == AutoKlasse.Luxusklasse)
            {
                BasistarifLabel.Visibility = Visibility.Visible;
                Basistarif.Visibility = Visibility.Visible;
            }
            else
            {
                BasistarifLabel.Visibility = Visibility.Hidden;
                Basistarif.Visibility = Visibility.Hidden;
            }
        }

        private void SetCarEditorVisible(bool visible)
        {
            if (visible)
            {
                AutoEditor.Visibility = Visibility.Visible;
                CarEditorEmptyHint.Visibility = Visibility.Collapsed;
                SaveButton.IsEnabled = true;
                DeleteButton.IsEnabled = true;
                UpdateBasistarifVisibility();
            }
            else
            {
                AutoEditor.Visibility = Visibility.Collapsed;
                CarEditorEmptyHint.Visibility = Visibility.Visible;
                SaveButton.IsEnabled = false;
                DeleteButton.IsEnabled = false;
            }
        }
    }
}
