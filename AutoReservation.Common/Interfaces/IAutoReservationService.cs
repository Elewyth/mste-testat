﻿using AutoReservation.Common.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace AutoReservation.Common.Interfaces
{
    [ServiceContract]
    public interface IAutoReservationService
    {
        #region Auto

        [OperationContract]
        int CreateAuto(AutoDto autoDto);

        [OperationContract]
        ICollection<AutoDto> ReadAllAutos();

        [OperationContract]
        ICollection<AutoDto> ReadAllAutoFields(AutoDto auto);

     /*   [OperationContract]
        AutoDto ReadAuto(int id);
*/
        [OperationContract]
        AutoDto ReadAutos(int id);

        [OperationContract]
        bool UpdateAuto(AutoDto autoDto);

        [OperationContract]
        bool DeleteAuto(AutoDto auto);

        #endregion
        #region  Kunde

        [OperationContract]
        int CreateKunde(KundeDto kunde);

        [OperationContract]
        ICollection<KundeDto> ReadAllKunden();

      /*  [OperationContract]
        KundeDto ReadKunde(int id);
*/
        [OperationContract]
        ICollection<KundeDto> ReadAllKundeFields(KundeDto kunde);

        [OperationContract]
        KundeDto ReadKunden(int id);

        [OperationContract]
        bool UpdateKunde(KundeDto kunde);

        [OperationContract]
        bool DeleteKunde(KundeDto kunde);

        #endregion
        #region  Reservation

        [OperationContract]
        int CreateReservation(ReservationDto reservation);

        [OperationContract]
        ICollection<ReservationDto> ReadAllReservationen();

        [OperationContract]
        ICollection<ReservationDto> ReadAllReservationenForAuto(AutoDto auto);

        [OperationContract]
        ReservationDto ReadReservation(int id);

        [OperationContract]
        bool UpdateReservation(ReservationDto reservation);

        [OperationContract]
        bool DeleteReservation(ReservationDto reservation);

        #endregion
    }
}
