﻿using System;
using System.Runtime.Serialization;

namespace AutoReservation.Common.DataTransferObjects
{
    [DataContract]
    public class KundeDto
    {
        [DataMember]
        public String Nachname { get; set; }

        [DataMember]
        public String Vorname { get; set; }

        [DataMember]
        public DateTime Geburtsdatum { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int RowVersion { get; set; }

      

        [IgnoreDataMember]
        public string FullName
        {
            get
            {
                return $"{Nachname} {Vorname}";
            }
        }

        public override string ToString()
            => $"{Id}; {Nachname}; {Vorname}; {Geburtsdatum}; {RowVersion}";
    }
}
