﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.ServiceModel;

namespace AutoReservation.Common.DataTransferObjects.Faults
{
    [Serializable]
    [SuppressMessage("Microsoft.Design", "CA1032:ImplementStandardExceptionConstructors")]
    public class OptimisticConcurrencyFaultException<T>
        : FaultException
    {
        public T MergedEntity { get; set; }

        public OptimisticConcurrencyFaultException(string message, T mergedEntity) : base(message)
        {
            MergedEntity = mergedEntity;
        }
    }
}
