﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.ServiceModel;

namespace AutoReservation.Common.DataTransferObjects.Faults
{
    [Serializable]
    [SuppressMessage("Microsoft.Design", "CA1032:ImplementStandardExceptionConstructors")]
    public class InvalidDateRangeFaultException
        : FaultException
    {
        public InvalidDateRangeFaultException(string message) : base(message) { }
    }
}
