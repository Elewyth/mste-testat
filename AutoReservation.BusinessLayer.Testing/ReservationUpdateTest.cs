﻿using AutoReservation.BusinessLayer.Exceptions;
using AutoReservation.Dal.Entities;
using AutoReservation.TestEnvironment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace AutoReservation.BusinessLayer.Testing
{
    [TestClass]
    public class ReservationUpdateTest
    {
        private ReservationManager target;
        private ReservationManager Target => target ?? (target = new ReservationManager());

        [TestInitialize]
        public void InitializeTestData()
        {
            TestEnvironmentHelper.InitializeTestData();
        }

        [TestMethod]
        public void ListReservationsTest()
        {
            Assert.AreEqual(4, Target.List.Count());
        }

        [TestMethod]
        public void CreateReservationTest()
        {
            int reservations = Target.List.Count;
            int result = Target.Create(new Reservation
            {
                AutoId = 1,
                KundeId = 1,
                Von = DateTime.Now,
                Bis = DateTime.Now.AddDays(4)
            });
            Assert.AreEqual(reservations + 1, Target.List.Count);
            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public void UpdateReservationTest()
        {
            // arrange
            int reservations = Target.List.Count;
            var reservation = Target.List.First();
            reservation.Von = DateTime.Now;
            reservation.Bis = DateTime.Now.AddDays(4);
            var rowVersionBefore = reservation.RowVersion;

            // act
            bool result = Target.Update(reservation);

            // assert
            Assert.IsTrue(result);
            Assert.AreEqual(reservations, Target.List.Count);
            Assert.AreEqual(reservation.Von.ToString(), Target.Read(reservation.ReservationsNr).Von.ToString());
            Assert.AreEqual(reservation.Bis.ToString(), Target.Read(reservation.ReservationsNr).Bis.ToString());
            Assert.AreNotEqual(rowVersionBefore.ToString(), Target.Read(reservation.ReservationsNr).RowVersion.ToString());
        }

        [TestMethod]
        public void UpdateReservationTest_RowVersionMismatch()
        {
            // arrange
            var reservation = Target.List.First();
            reservation.Von = DateTime.Now;
            reservation.RowVersion++;

            // act
            try
            {
                Target.Update(reservation);
                Assert.Fail("Expected OptimisticConcurrencyException to be thrown");
            }
            catch (OptimisticConcurrencyException<Reservation> ex)
            {
                // assert
                Assert.AreNotEqual(reservation.Von, ex.MergedEntity.Von);
                Assert.AreEqual(reservation.Bis, ex.MergedEntity.Bis);
                Assert.AreEqual(reservation.RowVersion - 1, ex.MergedEntity.RowVersion);
                Assert.AreNotEqual(reservation.Von.ToString(), Target.Read(reservation.ReservationsNr).Von.ToString());
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void UpdateReservationTest_FailOnInsert()
        {
            Target.Update(new Reservation
            {
                AutoId = 1,
                KundeId = 1,
                Von = DateTime.Now,
                Bis = DateTime.Now.AddDays(4)
            });
        }

        [TestMethod]
        public void DeleteReservationTest()
        {
            // arrange
            int reservations = Target.List.Count();
            var reservation = Target.List.First();

            // act
            Target.Delete(reservation);

            // assert
            Assert.AreEqual(reservations - 1, Target.List.Count());
            Assert.IsNull(Target.Read(reservation.ReservationsNr));
        }
    }
}
