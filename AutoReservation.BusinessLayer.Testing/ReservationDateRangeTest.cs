﻿using AutoReservation.BusinessLayer.Exceptions;
using AutoReservation.Dal.Entities;
using AutoReservation.TestEnvironment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AutoReservation.BusinessLayer.Testing
{
    [TestClass]
    public class ReservationDateRangeTest
    {
        private ReservationManager target;
        private ReservationManager Target => target ?? (target = new ReservationManager());


        [TestInitialize]
        public void InitializeTestData()
        {
            TestEnvironmentHelper.InitializeTestData();
        }

        [TestMethod]
        public void ScenarioOkay01Test()
        {
            Target.Validate(new Reservation { Von = new DateTime(2017, 11, 20, 12, 0, 0), Bis = new DateTime(2017, 11, 21, 12, 0, 0) });
        }

        [TestMethod]
        public void ScenarioOkay02Test()
        {
            Target.Validate(new Reservation { Von = new DateTime(2017, 11, 20), Bis = new DateTime(2017, 11, 21) });
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidDateRangeException))]
        public void ScenarioNotOkay01Test()
        {
            Target.Validate(new Reservation { Von = new DateTime(2017, 11, 20, 12, 0, 0), Bis = new DateTime(2017, 11, 21, 11, 0, 0) });
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidDateRangeException))]
        public void ScenarioNotOkay02Test()
        {
            Target.Validate(new Reservation { Von = new DateTime(2017, 11, 20), Bis = new DateTime(2017, 11, 19) });
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidDateRangeException))]
        public void ScenarioNotOkay03Test()
        {
            Target.Validate(new Reservation { Von = new DateTime(2017, 11, 20), Bis = new DateTime(2017, 11, 20) });
        }
    }
}
