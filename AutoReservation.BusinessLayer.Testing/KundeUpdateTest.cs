﻿using AutoReservation.Dal.Entities;
using AutoReservation.TestEnvironment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace AutoReservation.BusinessLayer.Testing
{
    [TestClass]
    public class KundeUpdateTest
    {
        private KundeManager target;
        private KundeManager Target => target ?? (target = new KundeManager());


        [TestInitialize]
        public void InitializeTestData()
        {
            TestEnvironmentHelper.InitializeTestData();
        }

        [TestMethod]
        public void UpdateKundeTest()
        {
            // Assert.Inconclusive("Test not implemented.");

            int kunde = Target.List.Count;
            var kunden = Target.List.First();
            kunden.Nachname = "Nachname";
            kunden.Nachname = "Name";
            var rowVersionBefore = kunden.RowVersion;

            // act
            bool result = Target.Update(kunden);

            // assert
            Assert.IsTrue(result);
            Assert.AreEqual(kunde, Target.List.Count);
            Assert.AreEqual(kunden.Nachname.ToString(), Target.Read(kunden.Id).Nachname.ToString());
            Assert.AreEqual(kunden.Nachname.ToString(), target.Read(kunden.Id).Nachname.ToString());
            Assert.AreNotEqual(rowVersionBefore.ToString(), Target.Read(kunden.Id).RowVersion.ToString());

        }
    }
}
