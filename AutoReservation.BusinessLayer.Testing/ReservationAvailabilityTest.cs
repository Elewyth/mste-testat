﻿using AutoReservation.BusinessLayer.Exceptions;
using AutoReservation.Dal.Entities;
using AutoReservation.TestEnvironment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AutoReservation.BusinessLayer.Testing
{
    [TestClass]
    public class ReservationAvailabilityTest
    {
        private ReservationManager target;
        private ReservationManager Target => target ?? (target = new ReservationManager());


        [TestInitialize]
        public void InitializeTestData()
        {
            TestEnvironmentHelper.InitializeTestData();
        }

        [TestMethod]
        public void ScenarioOkay01Test()
        {
            Target.Validate(new Reservation { AutoId = 1, Von = new DateTime(2020, 1, 20), Bis = new DateTime(2020, 1, 21) });
        }

        [TestMethod]
        public void ScenarioOkay02Test()
        {
            Target.Validate(new Reservation { AutoId = 1, Von = new DateTime(2020, 1, 9), Bis = new DateTime(2020, 1, 10) });
        }

        [TestMethod]
        public void ScenarioOkay03Test()
        {
            Target.Validate(new Reservation { ReservationsNr = 1, AutoId = 1, Von = new DateTime(2020, 1, 15), Bis = new DateTime(2020, 1, 25) });
        }

        [TestMethod]
        [ExpectedException(typeof(AutoUnavailableException))]
        public void ScenarioNotOkay01Test()
        {
            Target.Validate(new Reservation { AutoId = 1, Von = new DateTime(2020, 1, 15), Bis = new DateTime(2020, 1, 25) });
        }

        [TestMethod]
        [ExpectedException(typeof(AutoUnavailableException))]
        public void ScenarioNotOkay02Test()
        {
            Target.Validate(new Reservation { AutoId = 1, Von = new DateTime(2020, 1, 5), Bis = new DateTime(2020, 1, 15) });
        }

        [TestMethod]
        [ExpectedException(typeof(AutoUnavailableException))]
        public void ScenarioNotOkay03Test()
        {
            Target.Validate(new Reservation { AutoId = 1, Von = new DateTime(2020, 1, 5), Bis = new DateTime(2020, 1, 25) });
        }
    }
}
