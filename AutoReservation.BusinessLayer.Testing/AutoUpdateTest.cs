﻿using AutoReservation.Dal.Entities;
using AutoReservation.TestEnvironment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

[assembly: CLSCompliant(true)]
namespace AutoReservation.BusinessLayer.Testing
{
    [TestClass]
    public class AutoUpdateTests
    {
        private AutoManager target;
        private AutoManager Target => target ?? (target = new AutoManager());


        [TestInitialize]
        public void InitializeTestData()
        {
            TestEnvironmentHelper.InitializeTestData();
        }

        [TestMethod]
        public void UpdateAutoTest()
        {
            //   Assert.Inconclusive("Test not implemented.");

            int auto = Target.List.Count;
            var autos = Target.List.First();
            autos.Marke = "Audi";
            autos.Marke = "Suzuki";
            var rowVersionBefore = autos.RowVersion;

            // act
            bool result = Target.Update(autos);

            // assert
            Assert.IsTrue(result);
            Assert.AreEqual(auto, Target.List.Count);
            Assert.AreEqual(autos.Marke.ToString(), Target.Read(autos.Id).Marke.ToString());
            Assert.AreEqual(autos.Marke.ToString(), Target.Read(autos.Id).Marke.ToString());
            Assert.AreNotEqual(rowVersionBefore.ToString(), Target.Read(autos.Id).RowVersion.ToString());
        }
    }
}
